<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
				'required' => true,
				'attr' => ['minlength' => 2, 'maxlength' => 100]
			])
			->add('lastName', TextType::class, [
				'required' => true,
				'attr' => ['minlength' => 2, 'maxlength' => 100],
			])
			->add('email', EmailType::class, [])
            ->add('telephone', TelType::class, [
				'attr' => [
					'placeholder' => '0_.__.__.__.__',
					'pattern' => '(0)[1-9][0-9]{8}',
					'minlength' => 10, 'maxlength' => 10
				]
			])
			->add('object', ChoiceType::class, [
				'choices' => [
					'Autres' => 'contactez-nous',
					'Contacter le service commercial' => 'contacter-le-service-commercial',
					'Contacter le service facturation' => 'contacter-le-service-facturation',
                    'Faire une demande sur une association ou un projet' => 'faire-une-demande',
					'Proposer une fonctionnalité' => 'proposer-une-fonctionnalite',
                    'Je signale un problème technique' => 'signaler-un-probleme',
                    'Faire une remarque sur le site' => 'faire-une-remarque',
                    'Être rappelé' => 'etre-rappele'
                ]
			])
			->add('message', TextareaType::class, [
				'attr' => ['minlength' => 10, 'maxlength' => 500]
			])
        ;
    }
}
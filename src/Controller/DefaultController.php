<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class DefaultController extends AbstractController
{
    /**
     * @Route("/contact/{type}", name="contact")
     * 
     */
    public function index(string $type, Request $request)
    {	
		$form = $this->createForm(ContactType::class);
		$form->handleRequest($request);
		
		/*if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($data);
			$entityManager->flush();
				
			return $this->redirectToRoute('contact');
		}*/

        return $this->render('layout/contact.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}